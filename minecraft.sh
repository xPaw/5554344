#!/bin/bash

SCREEN_NAME="minecraft-vanilla"
FILE_NAME="server.jar"
STARTUP_PARAMETERS=(-Xmx2G -Xms1G -Dlog4j.configurationFile=log4j2.xml)

BLUE="\033[1;34m"
GRAY="\033[0m"
GREEN="\033[1;32m"
RED="\033[0;31m"
PREFIX="\t${BLUE}[>>]${GRAY}"

IsServerRunning( )
{
	screen -ls | grep --quiet ${SCREEN_NAME}
}

Attach( )
{
	if IsServerRunning
	then
		screen -R ${SCREEN_NAME}
	else
		echo -e "${PREFIX} ${GREEN}Minecraft${GRAY} server is ${RED}NOT ${GRAY}running."
	fi
}

Start( )
{
	if IsServerRunning
	then
		echo -e "${PREFIX} ${GREEN}Minecraft${GRAY} server is already running."
	else
		echo -e "${PREFIX} Starting ${GREEN}Minecraft${GRAY} server."

		screen -dmS ${SCREEN_NAME} java "${STARTUP_PARAMETERS[@]}" -jar ${FILE_NAME} nogui
	fi
}

Stop( )
{
	if IsServerRunning
	then
		echo -e "${PREFIX} Stopping ${GREEN}Minecraft${GRAY} server..."

		screen -R ${SCREEN_NAME} -X stuff $'stop\015'

		while IsServerRunning
		do
			sleep 1
			seconds=$seconds+1

			if [[ $seconds -eq 3 ]]
			then
				echo -e "${PREFIX} It's taking a while to shutdown..."
			elif [[ $seconds -ge 10 ]]
			then
				echo -e "${PREFIX} Failed to shutdown server, sending quit to screen."

				screen -R ${SCREEN_NAME} -X quit
			elif [[ $seconds -ge 15 ]]
			then
				echo -e "${PREFIX} Failed to shutdown server."

				exit 1
			fi
		done

		echo -e "${PREFIX} OK."
	else
		echo -e "${PREFIX} ${GREEN}Minecraft${GRAY} server is ${RED}NOT ${GRAY}running."
	fi
}

Update( )
{
	if IsServerRunning
	then
		echo -e "${PREFIX} You can't update while ${GREEN}Minecraft${GRAY} server is running."
		echo -e "${PREFIX} Use ${GREEN}$(basename "$0") stop${GRAY} to stop the server"
	else
		MC_VERSION_URLS=$(curl -s https://launchermeta.mojang.com/mc/game/version_manifest.json | python -c 'import json,sys,base64;obj=json.load(sys.stdin); print obj["versions"][0]["url"]')
		MC_LATEST_SNAPSHOT=$(curl -s "$MC_VERSION_URLS" | python -c 'import json,sys,base64;obj=json.load(sys.stdin); print obj["downloads"]["server"]["url"]')

		echo -e "${PREFIX} Downloading ${GREEN}${MC_LATEST_SNAPSHOT}${GRAY}..."

		wget --quiet "$MC_LATEST_SNAPSHOT" --output-document "$FILE_NAME" && echo -e "${PREFIX} Downloaded!" || echo -e "${PREFIX} ${RED}Failed!"
	fi
}

case $1 in
	attach)
		Attach
	;;
	start)
		Start
	;;
	stop)
		Stop
	;;
	update)
		Update
	;;
	*)
		echo -e "\tUsage: ${GREEN}$(basename "$0")${GRAY} { start | stop | update | attach }"
		exit 1
	;;
esac
